/*
 * Author: Philipp Müller [thegreatwhiteshark@googlemail.com] (2018)
 *
 * http://www.hydrogen-music.org
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#ifndef HUMANIZER_H
#define HUMANIZER_H

#include <QtGui>
#if QT_VERSION >= 0x050000
#include <QtWidgets>
#endif

#include <hydrogen/object.h>
#include <hydrogen/globals.h>
#include "../EventListener.h"

class Button;
class ToggleButton;
class HumanizerLine;
class ComponentHumanizerLine;
class HumanizerBaseLine;

class Humanizer : public QWidget, public EventListener, public H2Core::Object{
	H2_OBJECT
	Q_OBJECT

public:
	/** Constructor */
	Humanizer( QWidget *pParent );
	~Humanizer();

	void showEvent( QShowEvent *ev );
	void hideEvent( QHideEvent *ev );
	void resizeEvent( QResizeEvent *ev );

public slots:
	/** Hide the advanced humanization interface by calling
	    HydrogenApp::showHumanizer() */
	void closeEvent( QCloseEvent *ev );
	void updateHumanizer();

private:
	QHBoxLayout				*m_pMainHBox;
	QWidget					*m_pMainPanel;
	HumanizerBaseLine			*m_pBaseLine;
	HumanizerLine 				*m_pHumanizerLine[MAX_INSTRUMENTS];
	std::map<int,ComponentHumanizerLine*>	m_pComponentHumanizerLine;
	QTimer			        	*m_pUpdateTimer;
	HumanizerLine*				createHumanizerLine( int nInstr );
	ComponentHumanizerLine*			createComponentHumanizerLine( int nCompoID );

};

#endif // HUMANIZER_H
