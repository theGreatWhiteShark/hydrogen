/*
 * Author: Philipp Müller [thegreatwhiteshark@googlemail.com] (2018)
 *
 * http://www.hydrogen-music.org
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include "../HydrogenApp.h"
#include "HumanizerLine.h"
#include "../Widgets/Button.h"
#include "../Mixer/Mixer.h"

#include <hydrogen/Preferences.h>

using namespace H2Core;

const char* HumanizerLine::__class_name = "HumanizerLine";

HumanizerLine::HumanizerLine( QWidget *parent, int nInstr ) :
	PixmapWidget( parent, __class_name ){
	
	INFOLOG( QString( "Create HumanizerLine of nr %1" ).arg( nInstr ) );

	m_nWidth = HUMANIZER_STRIP_WIDTH;
	m_nHeight = HUMANIZER_STRIP_HEIGHT;

	resize( m_nWidth, m_nHeight );
	setFixedSize( m_nWidth, m_nHeight );

	setPixmap( "/humanizer/humanizerStripBackground.png" );

	// Widget to display the instrument's name.
	m_pNameWidget = new InstrumentNameWidget( this );
	m_pNameWidget->move( 6, 128 );
	m_pNameWidget->setToolTip( trUtf8( "Instrument name (double click to edit)" ) );
	connect( m_pNameWidget, SIGNAL( doubleClicked() ),
		 this, SLOT( nameClicked() ) );
	connect( m_pNameWidget, SIGNAL( clicked() ), this,
		 SLOT( nameSelected() ) );
}

HumanizerLine::~HumanizerLine(){
	INFOLOG( "DESTROY" );
}

void HumanizerLine::updateHumanizerLine(){
}

void HumanizerLine::nameClicked(){
	emit instrumentNameClicked( this );
}

void HumanizerLine::nameSelected(){
	emit instrumentNameSelected( this );
}


const char *ComponentHumanizerLine::__class_name = "ComponentHumanizerLine";

ComponentHumanizerLine::ComponentHumanizerLine( QWidget *parent, int compoID ) :
	PixmapWidget( parent, __class_name ){
	__compoID = compoID;

	INFOLOG( QString( "Create ComponentnHumanizerLine of ID %1" ).arg( compoID ) );

	m_nWidth = HUMANIZER_STRIP_WIDTH;
	m_nHeight = HUMANIZER_STRIP_HEIGHT;

	resize( m_nWidth, m_nHeight );
	setFixedSize( m_nWidth, m_nHeight );

	setPixmap( "/humanizer/componentHumanizerStripBackground.png" );

	m_pNameWidget = new InstrumentNameWidget( this );
	m_pNameWidget->move( 6, 128 );
	m_pNameWidget->setToolTip( trUtf8( "Component name" ) );
}

ComponentHumanizerLine::~ComponentHumanizerLine(){
	INFOLOG( "DESTROY" );
}

void ComponentHumanizerLine::updateHumanizerLine(){
}


const char* HumanizerBaseLine::__class_name = "HumanizerBaseLine";

HumanizerBaseLine::HumanizerBaseLine( QWidget *parent )
	: PixmapWidget( parent, __class_name ){

	m_nHeight = HUMANIZER_STRIP_HEIGHT;
	m_nWidth = HUMANIZER_BASE_WIDTH;

	setMinimumSize( m_nWidth, m_nHeight );
	setMaximumSize( m_nWidth, m_nHeight );
	resize( m_nWidth, m_nHeight );
	QPalette defaultPalette;
	defaultPalette.setColor( QPalette::Background,
				 QColor( 58, 62, 72 ) );
	this->setPalette( defaultPalette );

	// Background picture
	setPixmap( "/humanizer/humanizerBaseBackground.png" );

	// The Power button to activate the humanizer.
	m_pPowerBtn = new ToggleButton( this,
					"/humanizer/button_power_on.png",
					"/humanizer/button_power_off.png",
					"/humanizer/button_power_over.png",
					QSize( 71, 12 ) );
	m_pPowerBtn->move( 3, 10 );
	connect( m_pPowerBtn, SIGNAL( clicked( Button* ) ), this,
		 SLOT( powerButtonClicked( Button* ) ) );
}

HumanizerBaseLine::~HumanizerBaseLine(){
	INFOLOG( "DESTROY" );
}

void HumanizerBaseLine::powerButtonClicked( Button *pBtn ){
	Preferences::get_instance()->
		setAdvancedHumanizerActivation( pBtn->isPressed() );
	( HydrogenApp::get_instance() )->getMixer()->updateMixer();
	
}
