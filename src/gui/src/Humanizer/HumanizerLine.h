/*
 * Author: Philipp Müller [thegreatwhiteshark@googlemail.com] (2018)
 *
 * http://www.hydrogen-music.org
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#ifndef HUMANIZERLINE_H
#define HUMANIZERLINE_H

#include <QtGui>
#if QT_VERSION >= 0x05000
#include <QtWidgets>
#endif

#include <hydrogen/object.h>
#include <hydrogen/globals.h>

#include "../Mixer/MixerLine.h"
#include "../Widgets/PixmapWidget.h"

class Button;

#define HUMANIZER_STRIP_WIDTH 57
#define HUMANIZER_STRIP_HEIGHT 410
#define HUMANIZER_BASE_WIDTH 23

class HumanizerLine : public PixmapWidget {
	H2_OBJECT
	Q_OBJECT
public:
	HumanizerLine( QWidget *parent, int nInstr );
	~HumanizerLine();

	void updateHumanizerLine();

	void setName( QString name ){ m_pNameWidget->setText( name ); }
	QString getName() { return m_pNameWidget->text(); }

signals:
	void instrumentNameClicked( HumanizerLine *ref );
	void instrumentNameSelected( HumanizerLine *ref );

public slots:
	void nameClicked();
	void nameSelected();

private:
	uint m_nWidth;
	uint m_nHeight;

	InstrumentNameWidget *m_pNameWidget;
};

class ComponentHumanizerLine : public PixmapWidget {
	H2_OBJECT
	Q_OBJECT

public:
	ComponentHumanizerLine( QWidget *parent, int compoID );
	~ComponentHumanizerLine();

	void updateHumanizerLine();
	void setName( QString name ){ m_pNameWidget->setText( name ); }
	QString getName() { return m_pNameWidget->text(); }

	int getCompoID(){ return __compoID; }

private:
	int __compoID;
	uint m_nWidth;
	uint m_nHeight;

	InstrumentNameWidget *m_pNameWidget;

};

class HumanizerBaseLine : public PixmapWidget {
	H2_OBJECT
	Q_OBJECT
public:
	HumanizerBaseLine( QWidget *parent );
	~HumanizerBaseLine();
			    
public slots:
	void powerButtonClicked( Button *pBtn );

private:
	uint m_nWidth;
	uint m_nHeight;

	ToggleButton *m_pPowerBtn;
};


#endif
