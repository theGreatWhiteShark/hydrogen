/*
 * Author: Philipp Müller [thegreatwhiteshark@googlemail.com] (2018)
 *
 * http://www.hydrogen-music.org
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include "Humanizer.h"
#include "HumanizerLine.h"

#include "../HydrogenApp.h"

#include <hydrogen/hydrogen.h>
#include <hydrogen/basics/song.h>
#include <hydrogen/basics/instrument.h>
#include <hydrogen/basics/instrument_component.h>
#include <hydrogen/basics/instrument_list.h>
#include <hydrogen/basics/drumkit_component.h>


using namespace H2Core;

#include <cassert>

const char *Humanizer::__class_name = "Humanizer";

Humanizer::Humanizer( QWidget *pParent )
	: QWidget( pParent ), Object( __class_name ){
	
	setWindowTitle( trUtf8( "Humanizer" ) );
	setMaximumHeight( HUMANIZER_STRIP_HEIGHT );
	setMinimumHeight( HUMANIZER_STRIP_HEIGHT );
	setFixedHeight( HUMANIZER_STRIP_HEIGHT );

	// Main Panel
	m_pMainHBox = new QHBoxLayout();
	m_pMainHBox->setSpacing( 0 );
	m_pMainHBox->setMargin( 0 );

	m_pMainPanel = new QWidget( nullptr );
	m_pMainPanel->resize( HUMANIZER_STRIP_WIDTH * MAX_INSTRUMENTS +
			      HUMANIZER_BASE_WIDTH, height() );
	m_pMainPanel->setLayout( m_pMainHBox );

	for ( uint ii = 0; ii < MAX_INSTRUMENTS; ++ii ){
		m_pHumanizerLine[ ii ] = nullptr;
	}

	m_pBaseLine = new HumanizerBaseLine( nullptr );
	m_pBaseLine->move( 0, 0 );

	// Layout
	QHBoxLayout *pLayout = new QHBoxLayout();
	pLayout->setSpacing( 0 );
	pLayout->setMargin( 0 );
	pLayout->addWidget( m_pBaseLine );
	this->setLayout( pLayout );
		
	m_pUpdateTimer = new QTimer( this );
	connect( m_pUpdateTimer, SIGNAL( timeout() ), this,
		 SLOT( updateHumanizer() ) );

	HydrogenApp::get_instance()->addEventListener( this );
}

Humanizer::~Humanizer(){
	m_pUpdateTimer->stop();
}

HumanizerLine *Humanizer::createHumanizerLine( int nInstr ){
	HumanizerLine *pHumanizerLine = new HumanizerLine( nullptr, nInstr );

	return pHumanizerLine;
}

ComponentHumanizerLine *Humanizer::createComponentHumanizerLine( int nCompoID ){
	ComponentHumanizerLine *pHumanizerLine =
		new ComponentHumanizerLine( nullptr, nCompoID );

	return pHumanizerLine;
}

void Humanizer::closeEvent( QCloseEvent *ev ){
	HydrogenApp::get_instance()->showHumanizer( false );
}

void Humanizer::updateHumanizer(){

	Hydrogen *pEngine = Hydrogen::get_instance();
	Song *pSong = pEngine->getSong();
	InstrumentList *pInstrList = pSong->get_instrument_list();
	std::vector<DrumkitComponent*> *compoList = pSong->get_components();

	int nInstruments = pInstrList->size();
	int nCompo = compoList->size();

	for ( unsigned nnInstr = 0; nnInstr < MAX_INSTRUMENTS;
	      ++nnInstr ){
		if ( nnInstr >= nInstruments ){
			// Unused instruments.
			if ( m_pHumanizerLine[ nnInstr ] ){
				delete m_pHumanizerLine[ nnInstr ];
				m_pHumanizerLine[ nnInstr ] = nullptr;

				int newWidth = HUMANIZER_STRIP_WIDTH *
					( nInstruments + nCompo );
				if ( m_pMainPanel->width() != newWidth ){
					m_pMainPanel->resize( newWidth, height() );
				}
			}
			continue;
		} else {
			if ( m_pHumanizerLine[ nnInstr ] == nullptr ){
				// The HumanizerLine does not exist
				// for this instrument yet. Son let's
				// create one.
				INFOLOG( QString( "Create HumanizerLine nr %1" ). arg( nnInstr ) );
				m_pHumanizerLine[ nnInstr ] == createHumanizerLine( nnInstr );
				m_pMainHBox->insertWidget( nnInstr, m_pHumanizerLine[ nnInstr ] );
				
				int newWidth = HUMANIZER_STRIP_WIDTH * ( nInstruments + nCompo );
				if ( m_pMainPanel->width() != newWidth ){
					m_pMainPanel->resize( newWidth, height() );
				}
			}

			HumanizerLine *pLine = m_pHumanizerLine[ nnInstr ];
			Instrument *pInstr = pInstrList->get( nnInstr );
			assert( pInstr );

			pLine->updateHumanizerLine();
		}
	}

	for ( std::vector<DrumkitComponent*>::iterator it = compoList->begin();
	      it != compoList->end(); ++it ){
		DrumkitComponent *pCompo = *it;

		if ( m_pComponentHumanizerLine.find( pCompo->get_id() ) ==
		     m_pComponentHumanizerLine.end() ){
			// The HumanizerLine does not exists yet.
			INFOLOG( "Create ComponentHumanizerLine" );
			m_pComponentHumanizerLine[ pCompo->get_id() ] =
				createComponentHumanizerLine( pCompo->get_id() );
			m_pMainHBox->addWidget( m_pComponentHumanizerLine[ pCompo->get_id() ] );

			int newWidth = HUMANIZER_STRIP_WIDTH * ( nInstruments + nCompo );
			if ( m_pMainPanel->width() != newWidth ){
				m_pMainPanel->resize( newWidth, height() );
			}
		}

		QString sName = pCompo->get_name();
		ComponentHumanizerLine *pLine = m_pComponentHumanizerLine[ pCompo->get_id() ];
		pLine->setName( sName );
		pLine->updateHumanizerLine();
	}

	if ( compoList->size() < m_pComponentHumanizerLine.size() ){
		std::vector<int> *pIDsToDelete = new std::vector<int>();
		for ( std::map<int, ComponentHumanizerLine*>::iterator iit =
			      m_pComponentHumanizerLine.begin();
		      iit != m_pComponentHumanizerLine.end(); ++iit ){
			bool bFoundExistingRelatedComponent = false;
			for ( std::vector<DrumkitComponent*>::iterator jjt = compoList->begin();
			      jjt != compoList->end(); ++jjt ){
				DrumkitComponent *pAnotherCompo = *jjt;
				if ( pAnotherCompo->get_id() == iit->first ){
					bFoundExistingRelatedComponent = true;
					break;
				}
			}
			if ( !bFoundExistingRelatedComponent ){
				pIDsToDelete->push_back( iit->first );
			}
		}

		for ( std::vector<int>::iterator iit = pIDsToDelete->begin();
		      iit != pIDsToDelete->end(); ++iit ){
			int pCompoID = *iit;
			delete m_pComponentHumanizerLine[ pCompoID ];
			m_pComponentHumanizerLine.erase( pCompoID );

			int newWidth = HUMANIZER_STRIP_WIDTH * ( nInstruments + nCompo );
			if ( m_pMainPanel->width() != newWidth ){
				m_pMainPanel->resize( newWidth, height() );
			}
		}
	}
}	

void Humanizer::showEvent( QShowEvent *ev ){
	UNUSED( ev );
	updateHumanizer();
}

void Humanizer::hideEvent( QHideEvent *ev ){
	UNUSED( ev );
}

void Humanizer::resizeEvent( QResizeEvent *ev ){
	UNUSED( ev );
}
